package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener {

	private StatisticsPanel statisticsPanel;

	public StatisticsPanelAdapter(StatisticsPanel statisticsPanel) {
		this.statisticsPanel = statisticsPanel;
	}

	@Override
	public void courseHasChanged(Course course) {
		statisticsPanel.repaint();
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
	
}
